#!/usr/bin/env python3
# -*- coding: utf-8 -*-


"""
Autoresponse Telegram client
"""


import sys

import telethon


__author__      = "firolunis"
__copyright__   = "Copyright 2020"
__credits__     = ["firolunis"]
__license__     = "BSD 3-Clause"
__version__     = "0.0.1"
__maintainer__  = "firolunis"
__email__       = "firolunis@icloud.com"
__status__      = "Development"


API_ID, API_HASH = sys.argv[1:]


client = telethon.TelegramClient('autoresponse', API_ID, API_HASH)


@client.on(telethon.events.NewMessage())
async def handler(event):
    if type(event.message.chat) is telethon.tl.types.User:
        if event.message.voice:
            await event.respond('`Пользователь отключил голосовые сообщения.`')
            await event.message.delete(revoke=True)
        if event.message.video_note:
            await event.respond('`Пользователь отключил видеосообщения.`')
            await event.message.delete(revoke=True)


with client:
    print('Starting...')
    try:
        client.loop.run_forever()
    except KeyboardInterrupt:
        print('\nDetect Keyboard Interrupt! Exiting...')
    finally:
        client.disconnect()
